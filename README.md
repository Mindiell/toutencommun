# Share-ring

*English version below*

Share-ring est un outil permettant de prêter des objets et de rendre des coups de main
entre voisins.

*English version*

Share-ring is a tool for sharing objects and giving a hand among neighbors.


## Installation

### Development environment

Use a virtual environment

    python3 -m venv venv
    source venv/bin/activate

Install components

    pip install -r requirements-dev.txt

And start application

    flask run


### Production environment

Use a virtual environment

    python3 -m venv venv
    source venv/bin/activate

Install components

    pip install -r requirements.txt

And start application and distribute it through some proxy

    gunicorn --workers 2 --bind 0.0.0.0:5000 server


## Translation

Share-ring uses Flask-Babel for translations

## Extraction of strings to translate

    pybabel extract --ignore venv -F app/babel.cfg -o messages.pot ./

## Initialisation of a specific language

    pybabel init -i messages.pot -d translations -l fr

## Update of strings for all translated languages

    pybabel update -i messages.pot -d translations

## Compile each languages to use them

    pybabel compile -d translations

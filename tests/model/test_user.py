# encoding: utf-8

from app.model.user import UserModel


def test_empty_users_by_default(app):
    users = UserModel.query.all()
    assert len(users) == 0


def test_new_user_is_not_active(app):
    user = UserModel(login="user_test", password="user_password")
    user.save()
    assert user.active == False
    assert user.is_active == False


def test_new_user_is_not_admin(app):
    user = UserModel(login="user_test", password="user_password")
    user.save()
    assert user.admin == False


def test_unknown_user_is_anonymous(app):
    user = UserModel(login="user_test", password="user_password")
    assert user.is_anonymous == True
    user.save()
    assert user.is_anonymous == False

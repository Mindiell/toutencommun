# encoding: utf-8

import os

import pytest

from app import db
from server import create_app


BASE_DIR = os.path.abspath(os.path.dirname(__file__))

@pytest.fixture()
def app():
    # Specific settings for testing
    application = create_app({
        "SECRET_KEY": "Secret key for testing",
        "SQLALCHEMY_DATABASE_URI": "sqlite:///" + os.path.join(BASE_DIR, "db.sqlite3"),
        "BCRYPT_ROUNDS": 4,
        "TESTING": True,
        "WTF_CSRF_ENABLED": False,
    })
    with application.app_context():
        # Create a new database
        db.create_all()
        # other setup can go here
        yield application
        # clean up / reset resources here
        db.drop_all()


@pytest.fixture()
def client(app):
    return app.test_client()

# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import HiddenField, TextAreaField
from wtforms.validators import DataRequired


class DiscussionForm(FlaskForm):
    message = TextAreaField(
        _("Message"), render_kw={"rows": 6}, validators=[DataRequired()]
    )


class MessageForm(FlaskForm):
    discussion_id = HiddenField(validators=[DataRequired()])
    message = TextAreaField(
        _("Réponse"), render_kw={"rows": 6}, validators=[DataRequired()]
    )

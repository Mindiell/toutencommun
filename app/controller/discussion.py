# encoding: utf-8

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required

from app.controller.controller import Controller
from app.form.discussion import DiscussionForm, MessageForm
from app.model.discussion import DiscussionModel, MessageModel
from app.model.proposal import ProposalModel


class Discussion(Controller):
    @login_required
    def list(self):
        g.discussions = (
            DiscussionModel.query.join(
                ProposalModel, ProposalModel.id == DiscussionModel.proposal_id
            )
            .filter(DiscussionModel.active == True)
            .filter(
                (DiscussionModel.user_id == current_user.id)
                | (ProposalModel.owner_id == current_user.id)
            )
            .order_by(DiscussionModel.created.desc())
            .all()
        )

        return render_template("discussion/list.html")

    @login_required
    def new(self, id):
        g.proposal = ProposalModel.query.get(id)
        g.form = DiscussionForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                discussion = DiscussionModel(
                    proposal_id=id,
                    user_id=current_user.id,
                )
                discussion.save()
                message = MessageModel(
                    discussion_id=discussion.id,
                    user_id=current_user.id,
                    message=g.form.message.data,
                )
                message.save()
                flash(_("Votre message a été transmis"), "success")
                return redirect(url_for("discussion.view", id=discussion.id))

        return render_template("discussion/new.html")

    @login_required
    def view(self, id):
        g.discussion = DiscussionModel.query.get(id)
        if (
            g.discussion.user_id != current_user.id
            and g.discussion.proposal.owner_id != current_user.id
        ):
            return redirect(url_for("discussion.list"))
        g.discussion.read()
        g.form = MessageForm()
        g.form.discussion_id.data = g.discussion.id
        return render_template("discussion/view.html")

    @login_required
    def send(self):
        g.form = MessageForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                discussion = DiscussionModel.query.get(g.form.discussion_id.data)
                if (
                    discussion.user_id != current_user.id
                    and discussion.proposal.owner_id != current_user.id
                ):
                    return redirect(url_for("discussion.list"))
                message = MessageModel(
                    discussion_id=g.form.discussion_id.data,
                    user_id=current_user.id,
                    message=g.form.message.data,
                )
                message.save()
                flash(_("Votre message a été transmis"), "success")

        return redirect(url_for("discussion.view", id=g.form.discussion_id.data))

    @login_required
    def delete(self, id):
        discussion = DiscussionModel.query.get(id)
        if discussion is not None:
            if (
                discussion.user_id != current_user.id
                and discussion.proposal.owner_id != current_user.id
            ):
                return redirect(url_for("discussion.list"))
            discussion.active = False
            discussion.save()

        return redirect(url_for("discussion.list"))

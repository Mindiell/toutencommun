# encoding: utf-8

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required

from app.controller.controller import Controller
from app.model.proposal import ProposalModel
from app.form.proposal import ProposalSearchForm


class Core(Controller):
    def index(self):
        return render_template("core/index.html")

    @login_required
    def search(self):
        q = request.args.get("q", "").strip()
        g.form = ProposalSearchForm()
        g.form.q.data = q
        if g.form.q.data != "":
            g.proposals = (
                ProposalModel.query.filter(
                    ProposalModel.title.like(f"%{q}%")
                    | ProposalModel.description.like(f"%{q}%")
                )
                .filter(
                    # Don't want to see my proposals
                    ProposalModel.owner_id
                    != current_user.id
                )
                .filter(
                    # Only for members of my communities
                    ProposalModel.owner_id.in_(current_user.members_of_communities)
                )
                .all()
            )

        return render_template("core/search.html")

    def who(self):
        return render_template("core/who.html")

    def why(self):
        return render_template("core/why.html")

    def manifesto(self):
        return render_template("core/manifesto.html")

    def cgu(self):
        return render_template("core/cgu.html")

    def rgpd(self):
        return render_template("core/rgpd.html")

    def legal(self):
        return render_template("core/legal.html")

    def charter(self):
        return render_template("core/charter.html")

    def faq(self):
        return render_template("core/faq.html")

    def contact(self):
        return render_template("core/contact.html")

    def status(self):
        return render_template("core/status.html")

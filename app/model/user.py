# encoding: utf-8

import bcrypt

from flask import current_app

from app.model.model import db, Model


def get_user(user_id):
    return UserModel.query.get(user_id)


class UserModel(db.Model, Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(100), unique=True)
    password_hash = db.Column(db.String(128), default="")
    email = db.Column(db.String(500), default="")
    active = db.Column(db.Boolean, default=False)
    admin = db.Column(db.Boolean, default=False)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.hashpw(
            password.encode("utf-8"),
            bcrypt.gensalt(rounds=int(current_app.config["BCRYPT_ROUNDS"])),
        ).decode("UTF-8")

    def check_password(self, password):
        return bcrypt.checkpw(
            password.encode("utf-8"),
            self.password_hash.encode("utf-8"),
        )

    @property
    def is_active(self):
        return self.active or False

    @property
    def is_anonymous(self):
        return self.id is None

    @property
    def is_authenticated(self):
        return self.id is not None

    @property
    def is_admin(self):
        return self.admin

    def get_id(self):
        return str(self.id)

    @property
    def main_address(self):
        for address in self.addresses:
            if address.main:
                return address

    @property
    def active_discussions(self):
        return [discussion for discussion in self.discussions if discussion.active] + [
            discussion
            for proposal in self.proposals
            for discussion in proposal.discussions
            if discussion.active
        ]

    @property
    def unread_discussions(self):
        discussions = 0
        for discussion in self.active_discussions:
            if len(discussion.unread_messages) > 0:
                discussions += 1

        return discussions

    @property
    def members_of_communities(self):
        return [
            member.id for community in self.communities for member in community.members
        ]

    def __repr__(self):
        return self.login


class MemberModel(db.Model, Model):
    __tablename__ = "member"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("UserModel", backref="membership", viewonly=True)
    community_id = db.Column(db.Integer, db.ForeignKey("community.id"))
    community = db.relationship("CommunityModel", backref="membership", viewonly=True)
    manager = db.Column(db.Boolean, default=False)

    @property
    def is_manager(self):
        return self.manager


class CommunityModel(db.Model, Model):
    __tablename__ = "community"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    members = db.relationship(
        "UserModel",
        secondary=MemberModel.__table__,
        backref="communities",
    )

    @property
    def manager(self):
        for membership in self.membership:
            if membership.is_manager:
                return membership.user

    @property
    def nb_proposals(self):
        total = 0
        for membership in self.membership:
            total += len(membership.user.proposals)
        return total

    def __repr__(self):
        return self.name

# encoding: utf-8
"""
This module imports modles in order to allow flask to find them.
"""

import app.model.admin
import app.model.user
import app.model.proposal
import app.model.discussion

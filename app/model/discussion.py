# encoding: utf-8

from datetime import datetime

from flask import current_app
from flask_login import current_user

from app.model.model import db, Model


class DiscussionModel(db.Model, Model):
    __tablename__ = "discussion"
    id = db.Column(db.Integer, primary_key=True)
    proposal_id = db.Column(db.Integer, db.ForeignKey("proposal.id"))
    proposal = db.relationship("ProposalModel", backref="discussions")
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("UserModel", backref="discussions")
    created = db.Column(db.DateTime)
    active = db.Column(db.Boolean, default=True)

    def on_before_create(self, model):
        self.created = datetime.now()

    @property
    def unread(self):
        return len(self.unread_messages) > 0

    @property
    def unread_messages(self):
        return [
            message
            for message in self.messages
            if (message.user_id != current_user.id and not message.read)
        ]

    @property
    def last_message(self):
        return self.messages[-1]

    def read(self):
        for message in self.unread_messages:
            if not message.read:
                message.read = True
                message.save()

    def __repr__(self):
        return f"{self.proposal.title} - {self.user.login}"


class MessageModel(db.Model, Model):
    __tablename__ = "message"
    id = db.Column(db.Integer, primary_key=True)
    discussion_id = db.Column(db.Integer, db.ForeignKey("discussion.id"))
    discussion = db.relationship("DiscussionModel", backref="messages")
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    published = db.Column(db.DateTime)
    message = db.Column(db.Text)
    read = db.Column(db.Boolean, default=False)

    def on_before_create(self, model):
        self.published = datetime.now()

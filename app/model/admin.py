# encoding: utf-8

from app.model.model import db, Model


class RuleModel(db.Model, Model):
    __tablename__ = "rule"
    id = db.Column(db.Integer, primary_key=True)
    rule = db.Column(db.Text)


class SettingsModel(db.Model, Model):
    __tablename__ = "settings"
    id = db.Column(db.Integer, primary_key=True)
    can_subscribe = db.Column(db.Boolean, default=True)
    can_create_community = db.Column(db.Boolean, default=True)
